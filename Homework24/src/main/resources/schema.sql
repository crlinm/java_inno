-- Создать schema.sql файл, который содержит описание таблиц и данных для этих таблиц
--
-- Товар
-- - id
-- - описание
-- - стоимость
-- - количество
--
-- Заказчик
-- - id
-- - имя/фамилия
--
-- Заказ
-- - id-товара (внешний ключ)
-- - id-заказчика (внешний ключ)
-- - дата заказа
-- - количество товаров
--
-- Написать 3-4 запроса на эти таблицы.


create table product (
                         id integer generated always as identity primary key,
                         description text,
                         cost numeric,
                         count integer
);


-- drop table orders, client;
-- drop table product cascade ;


create table client (
                        id integer generated always as identity  primary key,
                        name text,
                        surname text
);


create table orders (
                        product_id integer,
                        foreign key(product_id) references product(id),
                        client_id integer,
                        foreign key(client_id) references client(id),
                        created_at timestamptz,
                        amount integer
);


insert into product (description, cost, count) values ('Стиральная машина BOSCH WLX 20463', 28990, 2);
insert into product (description, cost, count) values ('Микроволновая печь SAMSUNG MW 87 HPR', 11990, 1);
insert into product (description, cost, count) values ('Посудомоечная машина BOSCH SGS 44E12RU', 24999, 3);
insert into product (description, cost, count) values ('Холодильник LG GC-051 SS', 35000, 3);


update product set count = 11 where id=2;


insert into client (name, surname) VALUES ('Alina', 'M');


insert into orders (product_id, client_id, created_at, amount)
values (2, 1, now(), 5);


select *
from product
where cost > 20000;


select *
from product;


select *
from client;


select *
from orders;


-- удаляю ошибочно добавленную запись дважды
delete from product where id = 5;


select count(*)
from product;


select *
from orders o
         left join product p on o.product_id = p.id
where o.amount > 1;


