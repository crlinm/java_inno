package com.alina;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public interface ProductsRepository {

    List<Product> findAll();

    List<Product> findAllByPrice(double price);

    void save(Product product);

    /**
     *
     * @param ordersCount
     * @return все товары по количеству заказов, в которых участвуют
     */
    List<Product> findAllByOrdersCount(int ordersCount);
}
