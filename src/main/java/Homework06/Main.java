package Homework06;

public class Main{

    /**
     * @param number число, для которого нужно вернуть индекс этого числа в массиве,
     *               если число в массиве отсутствует - вернуть -1.
     *               array - массив
     * @return индекс числа в массиве
     */
    public static int getIndex(int[] array, int number){
        int index = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == number){
                index = i;
            }
        }

        return index;
    }

    /**
     * Процедура, которая переместит все значимые элементы влево, заполнив нулевые
     * @param array -- массив, в котором перемещаем элементы
     */
    public static void moveZeros(int[] array){
        int k = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i]!=0){
                array[k]=array[i];
                k++;
            }
        }

        for (int i = k; i < array.length; i++) {
            array[i] = 0;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] a = {1, 5, 4, 453, 34};
        int index = getIndex(a, 34);
        System.out.println(index);

        int[] a2 = {1, 0, 34, 5, 0};
        moveZeros(a2);

        int[] a3 = {0, 1, 0, 0, 0, 34, 0,  51, 0, 4};
        moveZeros(a3);
    }
}
