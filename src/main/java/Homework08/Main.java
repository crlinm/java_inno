package Homework08;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Human[] humans = new Human[10];

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < humans.length; i++) {
            Human human = new Human();

            System.out.println("Enter name: ");
            String name = scanner.next();
            human.setName(name);

            System.out.println("Enter weight: ");
            float weight = scanner.nextFloat();
            human.setWeight(weight);

            humans[i] = human;
        }

        // сортируем людей в массиве по весу сортировкой выбором
        for (int i = 0; i < humans.length; i++) {
            float min = humans[i].getWeight();
            int minIndex = i;
            for (int j = i+1; j < humans.length; j++) {
                if (humans[j].getWeight()<min){
                    min = humans[j].getWeight();
                    minIndex = j;
                }
            }
            Human temp = humans[i];
            humans[i] = humans[minIndex];
            humans[minIndex] = temp;
        }

        for (int i = 0; i < humans.length; i++) {
            System.out.println(humans[i].getName() + " " + humans[i].getWeight());
        }
    }
}
