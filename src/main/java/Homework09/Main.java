package Homework09;

public class Main {
    public static void main(String[] args) {
        Figure figure = new Figure(0,0);
        Rectangle rectangle = new Rectangle(0,0,4,5);
        Square square = new Square(0,0, 4);
        Ellipse ellipse = new Ellipse(0, 0, 2, 1);
        Circle circle = new Circle(0, 0, 2);

        System.out.println(rectangle + " perimeter: " + rectangle.getPerimeter());
        System.out.println(square + " perimeter: " + square.getPerimeter());
        System.out.println(ellipse + " perimeter: " + ellipse.getPerimeter());
        System.out.println(circle + " perimeter: " + circle.getPerimeter());
    }
}
