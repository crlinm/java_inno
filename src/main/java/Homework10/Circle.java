package Homework10;

public class Circle extends Ellipse implements Movable {
    public Circle(double x, double y, double radius) {
        super(x, y, radius, radius);
    }

    /**
     * Получение периметра окружности
     * @return - возвращает периметр окружности
     */
    public double getPerimeter() {
        return 2 * Math.PI * getRadius1();
    }

    @Override
    public void moveTo(double x, double y) {
        this.setX(x);
        this.setY(y);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (radius: " + getRadius1() + ")";
    }
}
