package Homework10;

public class Ellipse extends Figure{
    private final double radius1;
    private final double radius2;

    public double getRadius1() {
        return radius1;
    }

    public double getRadius2() {
        return radius2;
    }

    public Ellipse(double x, double y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    /**
     * Приближенный расчёт периметра эллипса, используется вторая формула Рамануджана
     * @return - возвращает периметр эллипса
     */
    public double getPerimeter() {
        double denominator = 3 * Math.pow((radius1 - radius2) / (radius1 + radius2), 2);
        double divider = 10 + Math.sqrt(4 - denominator);
        return Math.PI * (radius1 + radius2) * (1 + denominator/divider);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (radius1: " + radius1 + ", radius2: " + radius2 + ")";
    }
}
