package Homework10;

public interface Movable {
    void moveTo(double x, double y);
}
