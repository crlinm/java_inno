package Homework11;

public class Logger {

    private static final Logger instance;

    void Log(String message){
        System.out.println(message);
    }

    static{
        instance = new Logger();
    }

    private Logger(){}

    public static Logger getInstance(){
        return instance;
    }
}
