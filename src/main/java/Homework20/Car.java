package Homework20;

public class Car {
    private String idCar;
    private String model;
    private String color;
    private int km;
    private int cost;

    public Car(String idCar, String model, String color, int km, int cost) {
        this.idCar = idCar;
        this.model = model;
        this.color = color;
        this.km = km;
        this.cost = cost;
    }

    public String getIdCar() {
        return idCar;
    }

    public void setIdCar(String idCar) {
        this.idCar = idCar;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
