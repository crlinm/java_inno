package Homework20;

import java.util.List;

public interface CarsRepository {
    List<Car> findAll() throws LoadCarException;

    void save(Car car) throws SaveCarException;

}
