package Homework20;

public class LoadCarException extends Throwable {
    public LoadCarException(String fileName, Throwable cause) {
        super("Ошибка загрузки автомобилей из файла " + fileName, cause);
    }
}
