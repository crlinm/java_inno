package Homework20;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Используя Java Stream API, вывести:
 * <p>
 * Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
 * Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
 * * Вывести цвет автомобиля с минимальной стоимостью. // min + map
 * * Среднюю стоимость Camry *
 */
public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryImpl("cars.txt");
        try {
            List<Car> cars = carsRepository.findAll();
            cars.stream()
                    .filter(car -> (car.getColor().equals("Black") || car.getKm() == 0))
                    .forEach(car -> System.out.println("Номер автомобиля - " + car.getIdCar()));

            System.out.println();

            System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. " + cars.stream()
                    .filter(car -> (car.getCost() >= 700000 && car.getCost() <= 800000))
                    .map(car -> car.getModel())
                    .distinct()
                    .count());

        } catch (LoadCarException e) {
            System.out.println(e.getMessage() + ": " + e.getCause().getMessage());
        }

//        try {
//            Car car = new Car("o004aa111", "Camry", "Red",100,  45000);
//            carsRepository.save(car);
//        } catch (SaveCarException e) {
//            System.out.println(e.getMessage() + ": " + e.getCause().getMessage());
////            e.printStackTrace();
//        }
    }
}
