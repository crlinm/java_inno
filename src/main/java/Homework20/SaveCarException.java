package Homework20;

import java.io.IOException;

public class SaveCarException extends Throwable {
    public SaveCarException(String fileName, Throwable cause) {
        super("Ошибка сохранения новой записи в файл " + fileName, cause);
    }
}
